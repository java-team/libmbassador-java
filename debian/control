Source: libmbassador-java
Section: java
Priority: optional
Maintainer: Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Uploaders:
 Markus Koschany <apo@debian.org>
Build-Depends:
 debhelper-compat (= 13),
 default-jdk,
 libmaven-bundle-plugin-java,
 libservlet3.1-java,
 maven-debian-helper (>= 2.1)
Standards-Version: 4.5.1
Vcs-Git: https://salsa.debian.org/java-team/libmbassador-java.git
Vcs-Browser: https://salsa.debian.org/java-team/libmbassador-java
Homepage: https://github.com/bennidi/mbassador

Package: libmbassador-java
Architecture: all
Depends:
 ${maven:Depends},
 ${misc:Depends}
Suggests:
 ${maven:OptionalDepends}
Description: feature-rich Java event bus optimized for high-throughput
 MBassador is a light-weight, high-performance event bus implementing the
 publish subscribe pattern. It is designed for ease of use and aims to be
 feature rich and extensible while preserving resource efficiency and
 performance.
 .
 The core of MBassador is built around a custom data structure that provides
 non-blocking reads and minimized lock contention for writes such that
 performance degradation of concurrent read/write access is minimal.
